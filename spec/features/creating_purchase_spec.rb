require 'rails_helper'

RSpec.feature 'Creating purchase' do
  scenario 'Creating a purchase successfully' do
    visit '/purchases'
    click_link 'New purchase'
    fill_in :purchase_name, with: 'Shoes'
    fill_in :purchase_cost, with: 100
    click_button 'Create Purchase'

    expect(page).to have_content('Purchase was successfully created')
  end

  scenario 'Creating a purchase without a cost' do
    visit '/purchases'
    click_link 'New purchase'
    fill_in :purchase_name, with: 'Shoes'
    click_button 'Create Purchase'

    expect(page).to have_content('1 error prohibited this purchase from being saved:')
    expect(page).to have_content('Cost is not a number')
  end
end